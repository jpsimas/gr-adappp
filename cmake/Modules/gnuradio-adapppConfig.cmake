find_package(PkgConfig)

PKG_CHECK_MODULES(PC_GR_ADAPPP gnuradio-adappp)

FIND_PATH(
    GR_ADAPPP_INCLUDE_DIRS
    NAMES gnuradio/adappp/api.h
    HINTS $ENV{ADAPPP_DIR}/include
        ${PC_ADAPPP_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /var/empty/local/include
          /var/empty/include
)

FIND_LIBRARY(
    GR_ADAPPP_LIBRARIES
    NAMES gnuradio-adappp
    HINTS $ENV{ADAPPP_DIR}/lib
        ${PC_ADAPPP_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /var/empty/local/lib
          /var/empty/local/lib64
          /var/empty/lib
          /var/empty/lib64
          )

include("${CMAKE_CURRENT_LIST_DIR}/gnuradio-adapppTarget.cmake")

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(GR_ADAPPP DEFAULT_MSG GR_ADAPPP_LIBRARIES GR_ADAPPP_INCLUDE_DIRS)
MARK_AS_ADVANCED(GR_ADAPPP_LIBRARIES GR_ADAPPP_INCLUDE_DIRS)
