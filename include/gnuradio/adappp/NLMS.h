/* -*- c++ -*- */
/*
 * Copyright 2024 Joao P O Simas.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_ADAPPP_NLMS_H
#define INCLUDED_ADAPPP_NLMS_H

#include <gnuradio/adappp/api.h>
#include <gnuradio/block.h>
#include <string>

namespace gr {
  namespace adappp {

    /*!
     * \brief <+description of block+>
     * \ingroup adappp
     *
     */
    class ADAPPP_API NLMS : virtual public gr::block
    {
     public:
      typedef std::shared_ptr<NLMS> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of adappp::NLMS.
       *
       * To avoid accidental use of raw pointers, adappp::NLMS's
       * constructor is in a private implementation
       * class. adappp::NLMS::make is the public interface for
       * creating new instances.
       */
      static sptr make(int N, int decim, float mu, float eps = 1e-6, int frameSize = 0, bool initialState = true, std::string tagName = "", int waitSize = 0);
    };

  } // namespace adappp
} // namespace gr

#endif /* INCLUDED_ADAPPP_NLMS_H */
