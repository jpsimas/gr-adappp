/* -*- c++ -*- */
/*
 * Copyright 2024 Joao P O Simas.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_ADAPPP_VECTORSELECTOR_H
#define INCLUDED_ADAPPP_VECTORSELECTOR_H

#include <gnuradio/adappp/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
  namespace adappp {

    /*!
     * \brief <+description of block+>
     * \ingroup adappp
     *
     */
    class ADAPPP_API VectorSelector : virtual public gr::sync_block
    {
     public:
      typedef std::shared_ptr<VectorSelector> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of adappp::VectorSelector.
       *
       * To avoid accidental use of raw pointers, adappp::VectorSelector's
       * constructor is in a private implementation
       * class. adappp::VectorSelector::make is the public interface for
       * creating new instances.
       */
      static sptr make(int vectorSize);
    };

  } // namespace adappp
} // namespace gr

#endif /* INCLUDED_ADAPPP_VECTORSELECTOR_H */
