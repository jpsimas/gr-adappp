/* -*- c++ -*- */
/*
 * Copyright 2024 Joao P O Simas.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_ADAPPP_LMS_H
#define INCLUDED_ADAPPP_LMS_H

#include <gnuradio/adappp/api.h>
#include <gnuradio/block.h>

namespace gr {
  namespace adappp {

    /*!
     * \brief <+description of block+>
     * \ingroup adappp
     *
     */
    class ADAPPP_API LMS : virtual public gr::block
    {
    public:
      typedef std::shared_ptr<LMS> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of adappp::LMS.
       *
       * To avoid accidental use of raw pointers, adappp::LMS's
       * constructor is in a private implementation
       * class. adappp::LMS::make is the public interface for
       * creating new instances.
       */
      static sptr make(int N, int decim, float mu);
    protected:
    };

  } // namespace adappp
} // namespace gr

#endif /* INCLUDED_ADAPPP_LMS_H */
