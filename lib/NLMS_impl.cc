/* -*- c++ -*- */
/*
 * Copyright 2024 Joao P O Simas.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "NLMS_impl.h"
#include <gnuradio/io_signature.h>

#include <volk/volk.h>

namespace gr {
  namespace adappp {

    using input_type = gr_complex;

    using output_type = gr_complex;
    NLMS::sptr NLMS::make(int N, int decim, float mu, float eps, int frameSize, bool initialState, std::string tagName, int waitSize)
    {
      return gnuradio::make_block_sptr<NLMS_impl>(N, decim, mu, eps, frameSize, initialState, tagName, waitSize);
    }


    /*
     * The private constructor
     */
    NLMS_impl::NLMS_impl(int N, int decim, float mu, float eps, int frameSize, bool initialState, std::string tagName, int waitSize)
      : N(N),
	decim(decim),
	mu(mu),
	eps(eps),
	frameSize(frameSize),
	enabled(initialState),
	count(0),
	tagName(tagName),
	waitSize(waitSize),
	gr::block("linear_equalizer",
		  gr::io_signature::make(2, 2, sizeof(gr_complex)),
		  gr::io_signature::make3(2, 3, sizeof(gr_complex), sizeof(gr_complex), N * sizeof(gr_complex))) {
      
      wConj = (gr_complex *)volk_malloc(N*sizeof(gr_complex), volk_get_alignment());

      memset(wConj, 0, N*sizeof(gr_complex));

      set_tag_propagation_policy(TPP_DONT);
    }

    /*
     * Our virtual destructor.
     */
    NLMS_impl::~NLMS_impl() {
      free(wConj);
    }

    void
    NLMS_impl::forecast (int noutput_items, gr_vector_int &ninput_items_required) {
      ninput_items_required[0] = (noutput_items + N - 1)*decim;
      ninput_items_required[1] = noutput_items;
    }

    int NLMS_impl::general_work(int noutput_items,
			       gr_vector_int& ninput_items,
			       gr_vector_const_void_star& input_items,
			       gr_vector_void_star& output_items)
    {
      auto u = static_cast<const input_type*>(input_items[0]);
      auto d = static_cast<const input_type*>(input_items[1]);
      auto y = static_cast<output_type*>(output_items[0]);
      auto err = static_cast<output_type*>(output_items[1]);
      auto wOut = static_cast<output_type*>(output_items[2]);

      // Do <+signal processing+>
	
      if(enabled) {
	count += noutput_items;

	const int nProduced = (count < frameSize)? noutput_items : (frameSize - (count - noutput_items));
	
	for(auto i = 0; i < nProduced; i++) {
	  // y[i] = w^T u
	  volk_32fc_x2_dot_prod_32fc(y + i, u + i*decim, wConj, N);

	  err[i] = d[i] - y[i];
	
	  gr_complex normCompl;
	  volk_32fc_x2_conjugate_dot_prod_32fc(&normCompl, u + i*decim, u + i*decim, N);
	  const float norm = std::real(normCompl);
	
	  const gr_complex muE = mu*err[i]/(norm + eps);
	  // w = w + mu*e*(u^*)
	  volk_32fc_x2_s32fc_multiply_conjugate_add_32fc(wConj, wConj, u + i*decim, muE, N);

	  if(output_items.size() > 2) {
	    memcpy(wOut + N*i, wConj, N * sizeof(gr_complex));
	    volk_32fc_conjugate_32fc(wOut + N*i, wOut + N*i, N);
	  }	
	}
	
	if((frameSize != 0) && (count >= frameSize)) {
	  enabled = false;
	  count = 0;
	}

	// Tell runtime system how many input items we consumed on
	// each input stream.
	consume(0, nProduced*decim);
	consume(1, nProduced);

	return nProduced;
	
      } else {
	std::vector<gr::tag_t> tags;
	// loop because tags vector is not ordered
	for(auto j = 0; j < noutput_items; j++) {
	  get_tags_in_range(tags, 0, nitems_read(0) + j, nitems_read(0) + j + 1, pmt::intern(tagName));
	  if(tags.size() != 0) {
	    if((count + (tags[0].offset - nitems_read(0))) > waitSize) {
	      enabled = true;
	      count = 0;
	      reset();

	      // send frame_begin tag
	      for(auto i = 0; i < 3; i++)
		add_item_tag(i, nitems_written(i), pmt::string_to_symbol("frame_begin"), pmt::from_long(frameSize));

	      break;
	    }
	  }
	}

	const int toConsume0 = (enabled? (tags[0].offset - nitems_read(0)) : noutput_items*decim);

	count += toConsume0;
	
	// Tell runtime system how many input items we consumed on
	// each input stream.
	consume(0, toConsume0);
	consume(1, toConsume0/decim);
	
	return 0;
      }
    }

    void
    NLMS_impl::reset() {
      memset(wConj, 0, N*sizeof(gr_complex));
    }

  } /* namespace adappp */
} /* namespace gr */
