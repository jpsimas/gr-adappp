/* -*- c++ -*- */
/*
 * Copyright 2024 Joao P O Simas.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <gnuradio/io_signature.h>
#include "VectorSelector_impl.h"

namespace gr {
  namespace adappp {

    VectorSelector::sptr
    VectorSelector::make(int vectorSize)
    {
      return gnuradio::make_block_sptr<VectorSelector_impl>(
        vectorSize);
    }


    /*
     * The private constructor
     */
    VectorSelector_impl::VectorSelector_impl(int vectorSize)
      : vectorSize(vectorSize),
	gr::sync_block("VectorSelector",
		       gr::io_signature::make2(2 /* min inputs */, 2 /* max inputs */, vectorSize * sizeof(gr_complex), sizeof(short)),
		       gr::io_signature::make(1 /* min outputs */, 1 /*max outputs */, sizeof(gr_complex)))
    {}

    /*
     * Our virtual destructor.
     */
    VectorSelector_impl::~VectorSelector_impl()
    {
    }

    int
    VectorSelector_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      auto in = static_cast<const gr_complex*>(input_items[0]);
      auto indexIn = static_cast<const short*>(input_items[1]);
      auto out = static_cast<gr_complex*>(output_items[0]);

      #pragma message("Implement the signal processing in your block and remove this warning")
      // Do <+signal processing+>
      for(auto i = 0; i < noutput_items; i++) {
	out[i] = in[indexIn[i]];
      }
      
      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace adappp */
} /* namespace gr */
