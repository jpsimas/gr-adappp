/* -*- c++ -*- */
/*
 * Copyright 2024 Joao P O Simas.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_ADAPPP_VECTORSELECTOR_IMPL_H
#define INCLUDED_ADAPPP_VECTORSELECTOR_IMPL_H

#include <gnuradio/adappp/VectorSelector.h>

namespace gr {
  namespace adappp {

    class VectorSelector_impl : public VectorSelector
    {
     private:
      // Nothing to declare in this block.
      int vectorSize;
     public:
      VectorSelector_impl(int vectorSize);
      ~VectorSelector_impl();

      // Where all the action really happens
      int work(
              int noutput_items,
              gr_vector_const_void_star &input_items,
              gr_vector_void_star &output_items
      );
    };

  } // namespace adappp
} // namespace gr

#endif /* INCLUDED_ADAPPP_VECTORSELECTOR_IMPL_H */
