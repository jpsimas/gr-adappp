/* -*- c++ -*- */
/*
 * Copyright 2024 Joao P O Simas.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <gnuradio/io_signature.h>
#include "LMS_impl.h"

namespace gr {
  namespace adappp {

    #pragma message("set the following appropriately and remove this warning")
    using input_type = float;
    #pragma message("set the following appropriately and remove this warning")
    using output_type = float;
    LMS::sptr
    LMS::make(int N, int decim, float mu)
    {
      return gnuradio::make_block_sptr<LMS_impl>(
        N, decim, mu);
    }


    /*
     * The private constructor
     */
    LMS_impl::LMS_impl(int N, int decim, float mu)
      : N(N),
	decim(decim),
	mu(mu),
	gr::block("linear_equalizer",
		  gr::io_signature::make(2, 2, sizeof(gr_complex)),
		  gr::io_signature::make3(2, 3, sizeof(gr_complex), sizeof(gr_complex), N * sizeof(gr_complex)),
		  decim) {
      
      w = (gr_complex *)volk_malloc(N*sizeof(gr_complex), volk_get_alignment());
    }

    /*
     * Our virtual destructor.
     */
    LMS_impl::~LMS_impl()
    {
    }

    void
    LMS_impl::forecast (int noutput_items, gr_vector_int &ninput_items_required) {
      ninput_items_required[0] = (noutput_items + N - 1)*decim;
      ninput_items_required[1] = noutput_items;
    }
    
    int
    LMS_impl::general_work (int noutput_items,
			    gr_vector_const_void_star &input_items,
			    gr_vector_void_star &output_items)
    {
      auto u = static_cast<const input_type*>(input_items[0]);
      auto d = static_cast<const input_type*>(input_items[1]);
      auto y = static_cast<output_type*>(output_items[0]);
      auto err = static_cast<output_type*>(output_items[1]);
      auto wOut = static_cast<output_type*>(output_items[2]);

      #pragma message("Implement the signal processing in your block and remove this warning")
      // Do <+signal processing+>

      for(auto i = 0; i < noutput_items; i++) {
	gr_complex yConj;
	// yConj = u* w
	volk_32fc_x2_conjugate_dot_prod_32fc(yConj, w, u + i*decim, N);

	y[i] = std::conj(yConj);

	err[i] = (d[i] - y[i]);
	
	const gr_complex muE = mu*err[i];
	// w = w + mu*e*(u^*)
	volk_32fc_x2_s32fc_multiply_conjugate_add2_32fc(w, w, u + i*decim, &muE, N);

	if(output_items.size() > 2)
	  memcpy(wOut + i, w, N * sizeof(gr_complex));
	
      }
      return noutput_items;
    }

  } /* namespace adappp */
} /* namespace gr */
