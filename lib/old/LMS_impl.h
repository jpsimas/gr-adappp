/* -*- c++ -*- */
/*
 * Copyright 2024 Joao P O Simas.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_ADAPPP_LMS_IMPL_H
#define INCLUDED_ADAPPP_LMS_IMPL_H

#include <gnuradio/adappp/LMS.h>

namespace gr {
  namespace adappp {

    class LMS_impl : public LMS
    {
     private:
      // Nothing to declare in this block.
      int N;
      int decim;
      float mu;

      gr_complex* w;
     public:
      LMS_impl(int N, int decim, float mu);
      ~LMS_impl();

      // Where all the action really happens
      void forecast (int noutput_items, gr_vector_int &ninput_items_required);

      int general_work(int noutput_items,
           gr_vector_int &ninput_items,
           gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items);

    };

  } // namespace adappp
} // namespace gr

#endif /* INCLUDED_ADAPPP_LMS_IMPL_H */
