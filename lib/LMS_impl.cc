/* -*- c++ -*- */
/*
 * Copyright 2024 Joao P O Simas.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "LMS_impl.h"
#include <gnuradio/io_signature.h>

#include <volk/volk.h>

namespace gr {
  namespace adappp {

    using input_type = gr_complex;

    using output_type = gr_complex;
    LMS::sptr LMS::make(int N, int decim, float mu)
    {
      return gnuradio::make_block_sptr<LMS_impl>(N, decim, mu);
    }


    /*
     * The private constructor
     */
    LMS_impl::LMS_impl(int N, int decim, float mu)
      : N(N),
	decim(decim),
	mu(mu),
	gr::block("linear_equalizer",
		  gr::io_signature::make(2, 2, sizeof(gr_complex)),
		  gr::io_signature::make3(2, 3, sizeof(gr_complex), sizeof(gr_complex), N * sizeof(gr_complex))) {
      
      wConj = (gr_complex *)volk_malloc(N*sizeof(gr_complex), volk_get_alignment());
      for(auto i = 0; i < N; i++)
	wConj[i] = gr_complex(0.0f, 0.0f);
    }

    /*
     * Our virtual destructor.
     */
    LMS_impl::~LMS_impl() {
      free(wConj);
    }

    void
    LMS_impl::forecast (int noutput_items, gr_vector_int &ninput_items_required) {
      ninput_items_required[0] = (noutput_items + N - 1)*decim;
      ninput_items_required[1] = noutput_items;
    }

    int LMS_impl::general_work(int noutput_items,
			       gr_vector_int& ninput_items,
			       gr_vector_const_void_star& input_items,
			       gr_vector_void_star& output_items)
    {
      auto u = static_cast<const input_type*>(input_items[0]);
      auto d = static_cast<const input_type*>(input_items[1]);
      auto y = static_cast<output_type*>(output_items[0]);
      auto err = static_cast<output_type*>(output_items[1]);
      auto wOut = static_cast<output_type*>(output_items[2]);

      // Do <+signal processing+>

      for(auto i = 0; i < noutput_items; i++) {
	// y[i] = w^T u
	volk_32fc_x2_dot_prod_32fc(y + i, u + i*decim, wConj, N);

	err[i] = d[i] - y[i];
	
	const gr_complex muE = mu*err[i];
	// w = w + mu*e*(u^*)
	volk_32fc_x2_s32fc_multiply_conjugate_add_32fc(wConj, wConj, u + i*decim, muE, N);

	if(output_items.size() > 2) {
	  memcpy(wOut + N*i, wConj, N * sizeof(gr_complex));
	  volk_32fc_conjugate_32fc(wOut + N*i, wOut + N*i, N);
	}
	
      }
      
      // Tell runtime system how many input items we consumed on
      // each input stream.
      consume(0, noutput_items*decim);
      consume(1, noutput_items);

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace adappp */
} /* namespace gr */
