/* -*- c++ -*- */
/*
 * Copyright 2024 Joao P O Simas.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_ADAPPP_NLMS_IMPL_H
#define INCLUDED_ADAPPP_NLMS_IMPL_H

#include <gnuradio/adappp/NLMS.h>

namespace gr {
namespace adappp {

class NLMS_impl : public NLMS
{
private:
  // Nothing to declare in this block.
  int N;
  int decim;
  float mu;
  float eps;

  gr_complex* wConj;

  bool enabled;
  int frameSize; // frame size in samples
  int count;
  int waitSize;

  std::string tagName;
public:
  NLMS_impl(int N, int decim, float mu, float eps, int frameSize = 0, bool initialState = false, std::string tagName = "", int waitSize = 0);
  ~NLMS_impl();

  // Where all the action really happens
  void forecast(int noutput_items, gr_vector_int& ninput_items_required);

  int general_work(int noutput_items,
		   gr_vector_int& ninput_items,
		   gr_vector_const_void_star& input_items,
		   gr_vector_void_star& output_items);

  void reset();
};

} // namespace adappp
} // namespace gr

#endif /* INCLUDED_ADAPPP_NLMS_IMPL_H */
